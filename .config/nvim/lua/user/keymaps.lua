-- mode, key, value
local opts = { noremap = true, silent = true }
local ter_opts = { silent = true }

local function map(m, k, v, options)
  vim.keymap.set(m, k, v, options)
end

-----------
-- Modes --
-----------

local normal      = "n"
local insert      = "i"
local visual      = "v"
local visualblock = "x"
local term        = "t"
local command     = "c"

------------
-- Normal --
------------
-- Better Window Navigation
map(normal, [[<C-h>]], [[<C-w>h]], opts)
map(normal, [[<C-j>]], [[<C-w>j]], opts)
map(normal, [[<C-k>]], [[<C-w>k]], opts)
map(normal, [[<C-l>]], [[<C-w>l]], opts)

-- Resize Windows with arrows
map(normal, [[<C-Left>]], [[:vertical-resize -2<CR>]], opts)
map(normal, [[<C-Down>]], [[:resize -2<CR>]], opts)
map(normal, [[<C-Up>]], [[:resize +2<CR>]], opts)
map(normal, [[<C-Right>]], [[:vertical-resize +2<CR>]], opts)

-- Navigate buffers
map(normal, [[<S-l>]], [[:bnext<CR>]], opts)
map(normal, [[<S-h>]], [[:bprevious<CR>]], opts)

-- Quickly Search whatever is in main register
map(normal, [[<Leader>/]], [[/<C-r>"<CR>]], opts)

-- Open System Verilog Module file under cursor
map(normal, [[<Leader>e]], [[yiw:e <C-r>".sv<CR>]])

-- Move to previous buffer and delete current buffer but keep window open
map(normal, [[<Leader>q]], [[:bprevious <Bar> bdelete #<CR>]])

---------
-- LSP --
---------
-- TODO: Setup so that, if LSP command for got next/ prev fails, equivalent Treesitter command attempts to execute
map(normal, [[<Leader>a]], [[<CMD>lua vim.lsp.buf.formatting()<CR>]], opts)
map(normal, [[<Leader>j]], [[<CMD>lua vim.diagnostic.goto_next()<CR>]], opts)
map(normal, [[<Leader>k]], [[<CMD>lua vim.diagnostic.goto_prev()<CR>]], opts)
map(normal, [[<Leader>d]], [[<CMD>lua vim.lsp.buf.definition()<CR>]], opts)
map(normal, [[<Leader>r]], [[<CMD>lua vim.lsp.buf.references()<CR>]], opts)
map(normal, [[<Leader>i]], [[<CMD>lua vim.lsp.buf.hover()<CR>]], opts)
map(normal, [[<Leader>s]], [[<CMD>lua vim.lsp.buf.signature_help()<CR>]], opts)
--
-- TODO: Implement more LSP keybinds (particularly ones for basic metals functionality)
map(normal, [[<C-n>]], [[<CMD>NvimTreeToggle<CR>]])

------------
-- Visual --
------------
-- holds visual mode when indenting from it
map(visual, [[<]], [[<gv]], opts)
map(visual, [[>]], [[>gv]], opts)

-- Move text up and down
map(visual, [[K]], [[:m '<-2<CR>gv-gv]], opts)
map(visual, [[J]], [[:m '>+1<CR>gv-gv]], opts)
map(visual, [[<Leader>a]], [[<CMD>lua vim.lsp.buf.range_formatting()<CR>]], opts) -- a for "align"

------------------
-- Visual Block --
------------------
map(visualblock, [[K]], [[:m '<-2<CR>gv-gv]], opts)
map(visualblock, [[J]], [[:m '>+1<CR>gv-gv]], opts)

--------------
-- Terminal --
--------------
map(term, "<C-h>", "<C-\\><C-N><C-w>h", ter_opts)
map(term, "<C-j>", "<C-\\><C-N><C-w>j", ter_opts)
map(term, "<C-k>", "<C-\\><C-N><C-w>k", ter_opts)
map(term, "<C-l>", "<C-\\><C-N><C-w>l", ter_opts)
map(term, "<ESC>", "<C-\\><C-N><ESC>", ter_opts)

-------------
-- Plugins --
-------------

-- Telescope
map([[n]], [[<Leader>f]], [[<cmd>Telescope find_files<cr>]], ter_opts)
map([[n]], [[<Leader>g]], [[<cmd>Telescope live_grep<cr>]], ter_opts)
