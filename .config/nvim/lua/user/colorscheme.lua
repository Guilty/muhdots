local command = vim.cmd -- maybe these should be global?
local option  = vim.opt

local colorscheme = 'gruvbox' --'melange'
require('gruvbox').setup({
  undercurl = true,
  underline = true,
  bold = true,
  italic = false,
  strikethrough = true,
  invert_selection = true,
  invert_signs = false,
  invert_tabline = false,
  invert_intend_guides = false,
  inverse = true,
  contrast = "soft",
  overrides = {},
})

option.background    = 'dark'
local status_ok, _ = pcall(command, "colorscheme " .. colorscheme)
if not status_ok then
  vim.notify("colorscheme" .. colorscheme .. " not found!")
  return
end
-- Transparancy
-- command([[highlight Normal guibg=NONE ctermbg=None]])
