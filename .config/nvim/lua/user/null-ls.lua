local status_ok, null_ls = pcall(require, 'null-ls')
if not status_ok then
  return
end

--null_ls.setup({debug = true})

local formatting = null_ls.builtins.formatting
local diagnostics = null_ls.builtins.diagnostics

-- TODO figure out how to control and trigger autoformatting command
null_ls.setup({
  debug = true,
  fallback_severity = 2,
  sources = {
    formatting.stylua,
    --formatting.verible_verilog_format,
  },
})

local status_ok, helpers = pcall(require, 'null-ls.helpers')
if not status_ok then
  return
end

local verilator_parse = helpers.diagnostics.from_pattern(
  [[(%%%a*).*%s:*(.-):%s*(.-):%s*(.-):%s*(.*)]],
  { 'severity', 'filename', 'row', 'col', 'message' }, {
  severities = {
    ['%Error']   = helpers.diagnostics.severities.error,
    ['%Warning'] = helpers.diagnostics.severities.warning,
    ['%Note']    = helpers.diagnostics.severities.information,
  },
})

local verilator_lint = { method = null_ls.methods.DIAGNOSTICS,
  factory = helpers.generator_factory,
  filetypes = { 'verilog', 'systemverilog' },
  generator = null_ls.generator({
    command = "verilator",
    args = {
      "--lint-only",
      "-Wall",
      "-Wpedantic",
      "-Wno-fatal",
      "-Wno-DECLFILENAME",
      "$FILENAME",
      "dpu_adaptor_package.sv",
      "-y",
      "$DIRNAME",
    },
    format = "line",
    to_temp_file = true,
    from_stderr = true,
    use_cache = false,
    multiple_files = true,
    on_output = function(line, params)
      local diagnostic = verilator_parse(line, params)
      if diagnostic then
        diagnostic.filename = params.bufname
      end
      return diagnostic
    end,
  }),
}
-- null_ls.register(verilator_lint)


local function split_str(str, delimiter)
  local r = {}
  for substr in (str .. delimiter):gmatch("(.-)" .. delimiter) do
    table.insert(r, substr)
  end
  return r
end

local function sandparser(params, done)
  local lint_info = {}
  local pattern   = "(.-):%s*File%s*'(.-)'%s*Line%s*(%d+)%s*%(char%s*(%d+)%)" -- pattern works !
  local str_seq   = split_str(params.output, '\n')

  for i = 13, #str_seq do
    local match = { str_seq[i]:match(pattern) }
    if match[1] then
      local lint_info_entry = {}
      --lint_info_entry.severity = match[1]
      lint_info_entry.file  = match[2]
      lint_info_entry.row   = match[3]
      lint_info_entry.col   = match[4]
      -- need to parse match[1] to convert to valid severity value
      -- Skip past illustrative lines (highlighted erroneous code snippets)
      -- if string contains ERROR then vim.diagnostic.severity.ERROR
      -- if string contains INFO  then vim.diagnostic.severity.INFO
      i                     = i + 2
      while not str_seq[i]:match("%s*%+%-*%^*%-+") do
        i = i + 1
      end
      i = i + 1

      -- first line post skip is message string
      lint_info_entry.message = str_seq[i]
      table.insert(lint_info, lint_info_entry)
    end
  end
  return done(lint_info)
end

local sandpiperlint = {
  method            = null_ls.methods.DIAGNOSTICS, -- for now running it every save
  factory           = helpers.generator_factory,
  filetypes         = { 'tl-verilog' },
  fallback_severity = vim.diagnostic.severity.ERROR,
  generator         = null_ls.generator({
    command      = 'sandpiper',
    args         = { '-i', '$FILENAME' }, -- sandpiper is essentially in "lint only" mode
    to_stdin     = false,
    to_temp_file = true,
    from_stdin   = true,
    from_stderr  = true,
    format       = 'raw',
    on_output    = sandparser
  })
}
null_ls.register(sandpiperlint)
