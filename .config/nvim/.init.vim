syntax enable
filetype plugin indent on

set number
set expandtab
set tabstop=2 
set shiftwidth=2
set nowrap

set completeopt=menu,menuone,noselect
autocmd VimResized * wincmd =

tnoremap <ESC> <C-\><C-n>

"" Plugins
source /home/guilty/.config/nvim/plugins.vim
" Plugin configurations
source /home/guilty/.config/nvim/vim-airline.vim

"" Colorscheme
colorscheme gruvbox
set background=dark
set termguicolors
" Enables Transparancy
highlight Normal guibg=NONE ctermbg=None

" TL-Verilog stuff
au BufRead,BufNewFile *.tlv set filetype=transactionlevelverilog
autocmd Filetype transactionlevelverilog setlocal tabstop=3
autocmd Filetype transactionlevelverilog setlocal shiftwidth=3
filetype plugin indent on
