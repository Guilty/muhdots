## Aliases
alias l='ls --color=auto'
alias ll='ls -la --color=auto'
alias ls='ls --color=auto'
alias p='clear && pfetch'
alias vi='nvim'
alias vim='nvim'
alias cat='bat'
alias bashrc='"$EDITOR" "$HOME/.bashrc" && source "$HOME/.bashrc"'
alias bash_aliases='$EDITOR "$HOME/.bash_aliases" && source "$HOME/.bash_aliases"'
alias gen_password='openssl rand -base64 32'
alias top='gotop'
alias ms='mw -y jordan@jordanmcconnon.xyz'
alias nb='newsboat --url-file="$XDG_CONFIG_HOME/newsboat/urls" --config-file="$XDG_CONFIG_HOME/newsboat/config"'
alias youtube-dl='yt-dlp -f best'
alias mail='neomutt'
alias tar-extract='tar -xzvf'
alias tar-compress='tar -czvf'
alias sandpiper='/home/guilty/hdl/sandpiper/SandPiper_1.9-2018_02_11-beta_distro/bin/sandpiper'

# lf is awkward and doesnt have a man page, this invokes the equivalent if i type man lf
# and invokes ordinary man otherwise
man() {
  if [[ $@ == "lf"  ]]; then
    command lf -doc | less
  else
    command man "$@"
  fi
}

lfcd () {
  # Make sure cache is present
  [ ! -d "$HOME/.cache/lf" ] && mkdir -p "$HOME/.cache/lf"

  # Cd functionality
  tmp="$(mktemp)"
  # -config "$XDG_CONFIG_HOME/lf/lfrc"
  lf -last-dir-path="$tmp" "$@"
  [ -f "$tmp" ] && dir="$(cat "$tmp")" && rm -f "$tmp" && \
    [ -d "$dir" ] && [ "$dir" != "$(pwd)" ] && cd "$dir"
}
alias lf='lfcd'

footssh() {
  [ $TERM = "foot" ] && TERM=linux ssh $@ || ssh $@
}
alias ssh=footssh
alias site='ssh -l root jordanmcconnon.xyz'
alias vnvim='nvim -u $HOME/.config/nvim/vinit.lua'
