# .bashrc

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

set -o vi
PS1='\[\033[0;0m\][\[\033[0;32m\]\u\[\033[0;0m\]@\[\033[0;31m\]\h \[\033[0;33m\]\W\[\033[0;0m\]]\n\$ '

## Aliases
source $HOME/.bash_aliases

## Environment
export TERMINAL=foot
export EDITOR=nvim
export BROWSER=firefox
export WALLPAPER=$HOME/pictures/wallpapers/sushi.jpg
export XDG_CONFIG_HOME=$HOME/.config
export XDG_RUNTIME_DIR=$HOME/.run/user/$(id -u)

test -z "$(ls -A)" && mkdir -p $XDG_RUNTIME_DIR && chmod 700 $XDG_RUNTIME_DIR
export XKB_DEFAULT_LAYOUT=gb
export XKB_DEFAULT_OPTIONS=caps:escape
# Somewhere to keep my little util scripts
export PATH=$PATH:$HOME/.local/bin
# Scripts relating to website maintenance
export PATH=$PATH:$HOME/website/scripts
# Sandpiper onto path
export PATH=$PATH:$HOME/hdl/sandpiper/SandPiper_1.9-2018_02_11-beta_distro/bin/
